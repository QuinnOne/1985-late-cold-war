proximity_to_finland = {
	add_opinion_modifier = {
		target = FIN
		modifier = diplomatic_proximity
	}
	reverse_add_opinion_modifier = {
		target = FIN
		modifier = diplomatic_proximity
	}
	if = { 
		limit = { has_government = salafism } FIN = { add_popularity = { ideology = salafism popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = autocracy } FIN = { add_popularity = { ideology = autocracy popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = nationalism } FIN = { add_popularity = { ideology = nationalism popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = monarchism } FIN = { add_popularity = { ideology = monarchism popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = oligarchy } FIN = { add_popularity = { ideology = oligarchy popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = conservatism } FIN = { add_popularity = { ideology = conservatism popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = liberalism } FIN = { add_popularity = { ideology = liberalism popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = liberalism } FIN = { add_popularity = { ideology = liberalism popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = social_democracy } FIN = { add_popularity = { ideology = social_democracy popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = liberalism } FIN = { add_popularity = { ideology = liberalism popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = neo_socialism } FIN = { add_popularity = { ideology = neo_socialism popularity = 0.03 } }
	}
	if = { 
		limit = { has_government = marxism_leninism } FIN = { add_popularity = { ideology = marxism_leninism popularity = 0.03 } }
	}
}

distance_from_finland = {
	add_opinion_modifier = {
		target = FIN
		modifier = diplomatic_distance
	}
	reverse_add_opinion_modifier = {
		target = FIN
		modifier = diplomatic_distance
	}
	if = { 
		limit = { has_government = salafism } FIN = { add_popularity = { ideology = salafism popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = autocracy } FIN = { add_popularity = { ideology = autocracy popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = nationalism } FIN = { add_popularity = { ideology = nationalism popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = monarchism } FIN = { add_popularity = { ideology = monarchism popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = oligarchy } FIN = { add_popularity = { ideology = oligarchy popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = conservatism } FIN = { add_popularity = { ideology = conservatism popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = liberalism } FIN = { add_popularity = { ideology = liberalism popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = liberalism } FIN = { add_popularity = { ideology = liberalism popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = social_democracy } FIN = { add_popularity = { ideology = social_democracy popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = liberalism } FIN = { add_popularity = { ideology = liberalism popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = neo_socialism } FIN = { add_popularity = { ideology = neo_socialism popularity = -0.03 } }
	}
	if = { 
		limit = { has_government = marxism_leninism } FIN = { add_popularity = { ideology = marxism_leninism popularity = -0.03 } }
	}
}