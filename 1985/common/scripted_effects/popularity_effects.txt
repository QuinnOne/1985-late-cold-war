ruling_party_support_+1 = {
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = 
		{
			ideology = marxism_leninism
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity =
		{
			ideology = oligarchy
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.01
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.01
		}
	}
}

ruling_party_support_+2 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = 
		{
			ideology = marxism_leninism
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.02
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.02
		}
	}
}

ruling_party_support_+3 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = 
		{
			ideology = marxism_leninism
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.03
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.03
		}
	}
}

ruling_party_support_+4 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = 
		{
			ideology = marxism_leninism
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.04
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.04
		}
	}
}

ruling_party_support_+5 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = {
			ideology = marxism_leninism
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.05
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.05
		}
	}
}

ruling_party_support_+6 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = {
			ideology = marxism_leninism
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = {
			ideology = market_socialism
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity =
		{
			ideology = neo_socialism
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.06
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.06
		}
	}
}

ruling_party_support_+7 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = 
		{
			ideology = marxism_leninism
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.07
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.07
		}
	}
}

ruling_party_support_+8 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = 
		{
			ideology = marxism_leninism
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.08
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.08
		}
	}
}

ruling_party_support_+9 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = 
		{
			ideology = marxism_leninism
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.09
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.09
		}
	}
}

ruling_party_support_+10 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = 
		{
			ideology = marxism_leninism
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.10
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.10
		}
	}
}

ruling_party_support_+15 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = 
		{
			ideology = marxism_leninism
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity = 
		{
			ideology = socialist_authoritarianism
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.15
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.15
		}
	}
}

ruling_party_support_+20 = 
{
	if = 
	{
		limit = { has_government = marxism_leninism }
		add_popularity = 
		{
			ideology = marxism_leninism
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = socialist_authoritarianism }
		add_popularity =
		{
			ideology = socialist_authoritarianism
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = market_socialism }
		add_popularity = 
		{
			ideology = market_socialism
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = neo_socialism }
		add_popularity = 
		{
			ideology = neo_socialism
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = social_democracy }
		add_popularity = 
		{
			ideology = social_democracy
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = liberalism }
		add_popularity = 
		{
			ideology = liberalism
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = conservatism }
		add_popularity = 
		{
			ideology = conservatism
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = oligarchy }
		add_popularity = 
		{
			ideology = oligarchy
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = monarchism }
		add_popularity = 
		{
			ideology = monarchism
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = autocracy }
		add_popularity = 
		{
			ideology = autocracy
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = nationalism }
		add_popularity = 
		{
			ideology = nationalism
			popularity = 0.20
		}
	}

	if = 
	{
		limit = { has_government = salafism }
		add_popularity = 
		{
			ideology = salafism
			popularity = 0.20
		}
	}
}