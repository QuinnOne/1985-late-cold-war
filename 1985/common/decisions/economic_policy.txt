economic_policy = {
	
	seize_the_means_of_production = {
		visible = {
			NOT = { has_government = marxism_leninism }
			marxism_leninism > 0.05
		}
		available = {
			NOT = { has_government = autocracy }
			NOT = { has_government = monarchism }
			NOT = { has_government = liberalism }
			OR = {
				social_democracy > 0.15
				neo_socialism > 0.15
				marxism_leninism > 0.15
			}
		}
		modifier = {
		}
		complete_effect = {
			add_stability = -0.02
			add_war_support = -0.02
			increase_marxism_leninism_influence_by_1 = yes
			add_popularity = {
				ideology = marxism_leninism
				popularity = 0.03
			}
			create_random_factory_CIV_1 = yes
		}
		
		icon = communism
		
		cost = 150
		days_re_enable = 1825
		
		ai_will_do = {
			base = 0
			modifier = { has_government = marxism_leninism add = 8 }
			modifier = { has_government = neo_socialism add = 2 }
		}
	}
	
	state_corporatist_reforms = {
		visible = {
			NOT = { has_government = autocracy }
			autocracy > 0.05
		}
		available = {
			NOT = { has_government = liberalism }
			NOT = { has_government = social_democracy }
			NOT = { has_government = liberalism }
			NOT = { has_government = neo_socialism }
			NOT = { has_government = liberalism }
			OR = {
				autocracy > 0.15
				nationalism > 0.15
			}
		}
		modifier = {
		}
		complete_effect = {
			add_stability = -0.02
			add_war_support = -0.02
			increase_autocracy_influence_by_1 = yes
			add_popularity = {
				ideology = autocracy
				popularity = 0.03
			}
			create_random_factory_MIL_1 = yes
		}
		
		icon = fascism
		
		cost = 150
		days_re_enable = 1825
		
		ai_will_do = {
			base = 0
			modifier = { has_government = autocracy add = 8 }
			modifier = { has_government = nationalism add = 2 }
		}
	}
	
}