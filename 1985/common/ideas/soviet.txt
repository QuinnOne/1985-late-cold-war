ideas = {

    country = {

        SOV_stagnant_economy = {

		allowed = { always = no }
			
		removal_cost = -1

           	modifier = {
                	industrial_capacity_factory = -0.05
			research_time_factor = 0.02
			consumer_goods_factor = 0.05
           	}
        }

	SOV_ongoing_afghan_war = {
	
		allowed = { always = no }

		removal_cost = -1

		modifier = {
			army_org_factor = -0.1
			army_morale_factor = -0.05
		}
	}

	SOV_Perestroika = {

		allowed = { always = no }

		removal_cost = -1

		modifier = {
			stability_factor = -0.05
			trade_opinion_factor = 0.05
			production_speed_buildings_factor = 0.05
		}
	}

	SOV_Andropov_System_Idea = {

		allowed = { always = no }
		
		removal_cost = -1

		modifier = {
			stability_factor = 0.05
			local_resources_factor = 0.05
			industrial_capacity_factory = -0.05
		}
	}

	SOV_Stalin_System_Idea = {

		allowed = { always = no }

		removal_cost = -1

		modifier = {
			stability_factor = -0.05
			consumer_goods_factor = -0.05
			marxism_leninism_drift = 0.02
		}
	}

	SOV_Chinese_Style_Economy_Idea = {

		allowed = { always = no }

		removal_cost = -1

		modifier = {
			stability_factor = -0.1
			trade_opinion_factor = 0.1
			production_speed_buildings_factor = 0.1
			neo_socialism_drift = 0.02	
		}
	}

	SOV_Planned_Economy_Restoration_Idea = {

		allowed = { always = no }

		removal_cost = -1

		modifier = {
			stability_factor = -0.05
			production_speed_buildings_factor = 0.1
			trade_opinion_factor = -0.1
			consumer_goods_factor = -0.05	
		}
	}

	SOV_Raised_Alcohol_Prices = {

		allowed = { always = no }

		removal_cost = -1

		modifier = {
			political_power_gain = -0.05
			consumer_goods_factor = -0.02	
		}
	}

	SOV_anti_alcohol_campaign = {

		allowed = { always = no }

		removal_cost = -1

		modifier = {
			political_power_gain = 0.05
			consumer_goods_factor = 0.02
		}
	}

	SOV_media_reform = {

		allowed = { always = no }

		removal_cost = -1

		modifier = {
			political_power_factor = 0.1
			stability_factor = 0.05	
		}
	}
			
    }
}