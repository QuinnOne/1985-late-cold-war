everyone_hates_the_islamic_state = {

	enable = {
		NOT = { tag = ISS }
		OR = {
			NOT = { has_government = salafism }
			has_country_flag = shia_islam
		}
	}
	
	abort = {
		has_government = salafism
		NOT = { has_country_flag = shia_islam }
	}
	
	ai_strategy = {
		type = antagonize
		id = "ISS"
		value = 1000
	}

}