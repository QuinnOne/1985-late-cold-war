default = {
	#navy = {
		#male = {
		#}
		#female = {
		#}
	#}
	
	#army = {
		#male = {
		#}
		#female = {
		#}
	#}
	
	male = {
		"gfx/leaders/leader_unknown.dds"
	}
	female = {
		"gfx/leaders/leader_unknown.dds"
	}
}

continent = {
	name = europe
	army = {
		male = {
			"gfx/leaders/Europe/general1.dds"
			"gfx/leaders/Europe/general2.dds"
			"gfx/leaders/Europe/general3.dds"
			"gfx/leaders/Europe/general4.dds"
			"gfx/leaders/Europe/general5.dds"
			"gfx/leaders/Europe/general6.dds"
		}
	}
	
	navy = {
		male = {
			"gfx/leaders/Europe/admiral1.dds"
			"gfx/leaders/Europe/admiral2.dds"
			"gfx/leaders/Europe/admiral3.dds"
		}
	}
	
	political = {
		marxism_leninism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
		}
		neo_socialism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
		}
		social_democracy = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
		}
		conservatism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
		}
		oligarchy = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
		}
		monarchism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
		}
		nationalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
		}
		autocracy = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
				"gfx/leaders/Europe/politician_autocracy1.dds"
				"gfx/leaders/Europe/politician_autocracy2.dds"
			}
		}
		salafism = {
			male = { 
				"gfx/leaders/Europe/politician_salafism1.dds"
				"gfx/leaders/Europe/politician_salafism2.dds"
			}
		}
	}

}

continent = {
	name = north_america
	army = {
		male = {
			"gfx/leaders/Europe/general1.dds"
			"gfx/leaders/Europe/general2.dds"
			"gfx/leaders/Europe/general3.dds"
			"gfx/leaders/Europe/general4.dds"
			"gfx/leaders/Europe/general5.dds"
			"gfx/leaders/Europe/general6.dds"
		}
	}
	
	navy = {
		male = {
			"gfx/leaders/Europe/admiral1.dds"
			"gfx/leaders/Europe/admiral2.dds"
			"gfx/leaders/Europe/admiral3.dds"
		}
	}
	
	political = {
		marxism_leninism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		neo_socialism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		social_democracy = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		conservatism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		oligarchy = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		monarchism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		nationalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		autocracy = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
				"gfx/leaders/Europe/politician_autocracy1.dds"
				"gfx/leaders/Europe/politician_autocracy2.dds"
				"gfx/leaders/Europe/politician_autocracy3.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		salafism = {
			male = { 
				"gfx/leaders/Europe/politician_salafism1.dds"
				"gfx/leaders/Europe/politician_salafism2.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
	}

}

continent = {
	name = south_america
	army = {
		male = {
			"gfx/leaders/South America/general1.dds"
		}
	}
	
	navy = {
		male = {
			"gfx/leaders/South America/general1.dds"
		}
	}
	
	political = {
		marxism_leninism = {
			male = { 
				"gfx/leaders/South America/politician_marxism_leninism1.dds"
			}
		}
		neo_socialism = {
			male = { 
				"gfx/leaders/South America/politician1.dds"
				"gfx/leaders/South America/politician2.dds"
				"gfx/leaders/South America/politician3.dds"
				"gfx/leaders/South America/politician4.dds"
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/South America/politician1.dds"
				"gfx/leaders/South America/politician2.dds"
				"gfx/leaders/South America/politician3.dds"
				"gfx/leaders/South America/politician4.dds"
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
		social_democracy = {
			male = { 
				"gfx/leaders/South America/politician1.dds"
				"gfx/leaders/South America/politician2.dds"
				"gfx/leaders/South America/politician3.dds"
				"gfx/leaders/South America/politician4.dds"
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/South America/politician1.dds"
				"gfx/leaders/South America/politician2.dds"
				"gfx/leaders/South America/politician3.dds"
				"gfx/leaders/South America/politician4.dds"
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/South America/politician1.dds"
				"gfx/leaders/South America/politician2.dds"
				"gfx/leaders/South America/politician3.dds"
				"gfx/leaders/South America/politician4.dds"
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
		conservatism = {
			male = { 
				"gfx/leaders/South America/politician1.dds"
				"gfx/leaders/South America/politician2.dds"
				"gfx/leaders/South America/politician3.dds"
				"gfx/leaders/South America/politician4.dds"
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
		oligarchy = {
			male = { 
				"gfx/leaders/South America/politician1.dds"
				"gfx/leaders/South America/politician2.dds"
				"gfx/leaders/South America/politician3.dds"
				"gfx/leaders/South America/politician4.dds"
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
		monarchism = {
			male = { 
				"gfx/leaders/South America/politician1.dds"
				"gfx/leaders/South America/politician2.dds"
				"gfx/leaders/South America/politician3.dds"
				"gfx/leaders/South America/politician4.dds"
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
		nationalism = {
			male = { 
				"gfx/leaders/South America/politician1.dds"
				"gfx/leaders/South America/politician2.dds"
				"gfx/leaders/South America/politician3.dds"
				"gfx/leaders/South America/politician4.dds"
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
		autocracy = {
			male = { 
				"gfx/leaders/South America/politician1.dds"
				"gfx/leaders/South America/politician2.dds"
				"gfx/leaders/South America/politician3.dds"
				"gfx/leaders/South America/politician4.dds"
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
		salafism = {
			male = { 
				"gfx/leaders/South America/politician_salafism1.dds" 
				"gfx/leaders/South America/politician_salafism2.dds" 
			}
			female = {
				"gfx/leaders/South America/politician_female1.dds"
			}
		}
	}

}

continent = {
	name = australia
	army = {
		male = {
			"gfx/leaders/Europe/general1.dds"
			"gfx/leaders/Europe/general2.dds"
			"gfx/leaders/Europe/general3.dds"
			"gfx/leaders/Europe/general4.dds"
			"gfx/leaders/Europe/general5.dds"
			"gfx/leaders/Europe/general6.dds"
		}
	}
	
	navy = {
		male = {
			"gfx/leaders/Europe/admiral1.dds"
			"gfx/leaders/Europe/admiral2.dds"
			"gfx/leaders/Europe/admiral3.dds"
		}
	}
	
	political = {
		marxism_leninism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		neo_socialism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		social_democracy = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		conservatism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		oligarchy = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		monarchism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		nationalism = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		autocracy = {
			male = { 
				"gfx/leaders/Europe/politician1.dds"
				"gfx/leaders/Europe/politician2.dds"
				"gfx/leaders/Europe/politician3.dds"
				"gfx/leaders/Europe/politician4.dds"
				"gfx/leaders/Europe/politician5.dds"
				"gfx/leaders/Europe/politician6.dds"
				"gfx/leaders/Europe/politician7.dds"
				"gfx/leaders/Europe/politician8.dds"
				"gfx/leaders/Europe/politician9.dds"
				"gfx/leaders/Europe/politician10.dds"
				"gfx/leaders/Europe/politician11.dds"
				"gfx/leaders/Europe/politician12.dds"
				"gfx/leaders/Europe/politician13.dds"
				"gfx/leaders/Europe/politician_autocracy1.dds"
				"gfx/leaders/Europe/politician_autocracy2.dds"
				"gfx/leaders/Europe/politician_autocracy3.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
		salafism = {
			male = { 
				"gfx/leaders/Europe/politician_salafism1.dds"
				"gfx/leaders/Europe/politician_salafism2.dds"
			}
			female = {
				"gfx/leaders/Europe/politician_female1.dds"
			}
		}
	}

}

continent = {
	name = africa
	army = {
		male = {
			"gfx/leaders/Africa/general1.dds"
			"gfx/leaders/Africa/general2.dds"
			"gfx/leaders/Africa/general3.dds"
		}
	}
	
	navy = {
		male = {
			"gfx/leaders/Africa/general1.dds"
			"gfx/leaders/Africa/general2.dds"
			"gfx/leaders/Africa/general3.dds"
		}
	}
	
	political = {
		marxism_leninism = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
		neo_socialism = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
		liberalism = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
		social_democracy = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
		liberalism = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
		liberalism = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
		conservatism = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
		oligarchy = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
		nationalism = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
		monarchism = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
		autocracy = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
				"gfx/leaders/Africa/general2.dds"
				"gfx/leaders/Africa/general3.dds"
			}
		}
		salafism = {
			male = {
				"gfx/leaders/Africa/politician1.dds"
				"gfx/leaders/Africa/politician2.dds"
				"gfx/leaders/Africa/politician3.dds"
			}
		}
	}


}

continent = {
	name = asia
	army = {
		male = {
			"gfx/leaders/Asia/general1.dds"
			"gfx/leaders/Asia/general2.dds"
		}
	}
	
	navy = {
		male = {
			"gfx/leaders/Asia/general1.dds"
			"gfx/leaders/Asia/general2.dds"
		}
	}

	political = {
		marxism_leninism = {
			male = { 
				"gfx/leaders/Asia/politician_marxism_leninism1.dds"
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
			}
		}
		neo_socialism = {
			male = { 
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
			}
		}
		social_democracy = {
			male = { 
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
			}
		}
		liberalism = {
			male = { 
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
			}
		}
		conservatism = {
			male = { 
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
			}
		}
		oligarchy = {
			male = { 
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
			}
		}
		monarchism = {
			male = { 
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
				"gfx/leaders/Asia/politician_monarchism1.dds"
				"gfx/leaders/Asia/politician_monarchism2.dds"
			}
		}
		nationalism = {
			male = { 
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
			}
		}
		autocracy = {
			male = { 
				"gfx/leaders/Asia/politician1.dds"
				"gfx/leaders/Asia/politician2.dds"
				"gfx/leaders/Asia/general1.dds"
				"gfx/leaders/Asia/general2.dds"
			}
		}
		salafism = {
			male = { 
				"gfx/leaders/Asia/politician2.dds"
			}
		}
	}	
}