
state={
	id=922
	name="STATE_922"
	resources={
	}

	history={
		owner = AFG
		buildings = {
			infrastructure = 2
			arms_factory = 1
		}
		victory_points = { 12769 1 }	#ghazni
		victory_points = { 12831 1 }	#khost
		victory_points = { 4893 10 }	#Herat
		victory_points = { 12871 1 }	#farah
		add_core_of = MUJ
		add_core_of = AFG

	}

	provinces={
		1995 2078 4893 5082 8053 8090 10789 12769 12788 12831 12871 13264 13266 13268 13269 13275 13279 13280 13281
	}
	manpower=5078451
	buildings_max_level_factor=1.000
	state_category = state_level_7
}