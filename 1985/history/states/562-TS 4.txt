
state={
	id=562
	name="STATE_562" # Okhotsk
	manpower = 321453
	state_category = state_level_0

	history={
		owner = SOV
		add_core_of = SOV
		add_core_of = RUS
		buildings = {
			infrastructure = 1
		}
	}

	provinces={
		1714 1805 1865 4810 4881 7772 10540 10572 10598 11607 12518 12551 12650 12683 
	}
}
