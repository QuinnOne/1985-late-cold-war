
state={
	id=243
	name="STATE_243"
	manpower = 876210
	
	state_category = state_level_5
	
	resources = {
		steel = 6
	}

	history={
		owner = SOV
		add_core_of = SOV
		add_core_of = RUS
		buildings = {
			infrastructure = 5
			air_base = 2
		}
	}

	provinces={
		223 236 3210 3278 3297 6243 9253 9351 11336 
	}
}
