﻿division_template = {
	name = "Infantry Unit"
	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 0 y = 4 }
	}
	
	support = {
		artillery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Mixed Division"

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		light_mechanized = { x = 0 y = 4 }
		modern_armor = { x = 1 y = 0 }
		modern_armor = { x = 1 y = 1 }
	}

	support = {
		artillery = { x = 0 y = 0 }
		recon = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Mountain Brigade"
	
	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 0 y = 3 }
		mountaineers = { x = 0 y = 4 }
	}
	
	support = {
		anti_tank = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Armored Brigade"
	
	regiments = {
		light_mechanized = { x = 0 y = 0 }
		modern_armor = { x = 1 y = 0 }
		modern_armor = { x = 1 y = 1 }
		modern_armor = { x = 1 y = 2 }
		modern_armor = { x = 1 y = 3 }
	}
	
	support = {
		artillery = { x = 0 y = 0 }
	}
}

units = {

	division = {
		name = "Republican Guard Brigade"
		location = 10737
		division_template = "Infantry Unit"
		start_experience_factor = 0.1
		force_equipment_variants = { infantry_equipment_0 = { owner = "AFG" } }
	}

	division = {
		name = "7th Armored Brigade"
		location = 5064
		division_template = "Armored Brigade"
		start_experience_factor = 0.1
		force_equipment_variants = { infantry_equipment_0 = { owner = "AFG" } }
	}

	division = {
		name = "15th Infantry Division"
		location = 5082
		division_template = "Infantry Unit"
		start_experience_factor = 0.1
		force_equipment_variants = { infantry_equipment_0 = { owner = "AFG" } }
	}

	division = {
		name = "11th Division"
		location = 10826
		division_template = "Mixed Division"
		start_experience_factor = 0.1
		force_equipment_variants = { infantry_equipment_0 = { owner = "AFG" } }
	}

	division = {
		name = "12th Division"
		location = 13282
		division_template = "Mixed Division"
		start_experience_factor = 0.1
		force_equipment_variants = { infantry_equipment_0 = { owner = "AFG" } }
	}

	division = {
		name = "14th Division"
		location = 12769
		division_template = "Mountain Brigade"
		start_experience_factor = 0.2
		force_equipment_variants = { infantry_equipment_0 = { owner = "AFG" } }
	}

	division = {
		name = "15th Division"
		location = 12831
		division_template = "Mountain Brigade"
		start_experience_factor = 0.2
		force_equipment_variants = { infantry_equipment_0 = { owner = "AFG" } }
	}

	division = {
		name = "18th Division"
		location = 12248
		division_template = "Mixed Division"
		start_experience_factor = 0.1
		force_equipment_variants = { infantry_equipment_0 = { owner = "SOV" }
		modern_tank_equipment_1 = { owner = "SOV" }
		armored_personnel_carrier_1 = { owner = "SOV" } }
	}

	division = {
		name = "20th  Division"
		location = 12877
		division_template = "Mixed Division"
		start_experience_factor = 0.1
		force_equipment_variants = { infantry_equipment_0 = { owner = "SOV" }
		modern_tank_equipment_1 = { owner = "SOV" }
		armored_personnel_carrier_1 = { owner = "SOV" } }
	}

	division = {
		name = "25th Division"
		location = 12831
		division_template = "Mixed Division"
		start_experience_factor = 0.1
		force_equipment_variants = { infantry_equipment_0 = { owner = "SOV" }
		modern_tank_equipment_1 = { owner = "SOV" }
		armored_personnel_carrier_1 = { owner = "SOV" } }
	}
	
}

instant_effect = {
	
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_0
			creator = "SOV"
		}
		requested_factories = 1
		progress = 0.1
		efficiency = 1
		amount = 25000
	}
	
}

air_wings = {
	267 = {
		jet_attack_equipment_1 = {
			owner = "SOV" 
			amount = 10
		}
	}
	267 = {
		jet_multirole_equipment_1 = {
			owner = "SOV" 
			amount = 5

		}
	}
	}