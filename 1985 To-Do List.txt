[COMPLETE] -countries_l_english.yml localisation fix (Need to give proper names to countries based on their iedology)

-Making and fixing starting leaders. Not the biggest priority, but still needs to be done.
Making a leader is fairly straightforward. To see an example, open ALB - Albania in history/countries, and see this:
create_country_leader = {
	name = "Ramiz Alia"
	ideology = marxism_leninism_id
	picture = "Ramiz_Alia.dds"
}
The ideology you choose for a leader is not the same as the ruling party's ideology name. Go to common/ideologies and open 00_socialist_ideologies and you'll see under marxism_leninism is the type marxism_leninism_id, which is what Ramiz Alia is.
The picture for the leader can be found in gfx/leaders/ALB.
I think for now we will only put in leaders of current ruling parties at the game's start. We can split this job between us in some way.

-Fix for State "61-Mecklenburg". I don't know how to fix it. Every time the owner country's tag is changed, the game stops running and there is nothing about it in error logs. We need to change the owner country and core to DDR.

-Adding an opinion/AI modifier for similar ideologies, kind of like Kaiserreich. Or at least something to make sure that NATO and Warsaw Pact don't dissolve themselves because there are different ideologies in each faction. I'll have to look more into this to see how to do it.

-The flags for every country also need to be updated with the ideology change. (Being worked on)

-We need to add in some new states eventually, such as West Berlin and Syrian Lebanon.

[COMPLETE] -countries_breakaways_l_english.yml needs a localization fix as well, exactly the same process as the other one. (Proper names still need to be put in as well)

[COMPLETE] -faction_names_l_english.yml also needs an update with the ideologies. I'll probably be working on this soon.

-Fixing technology. We will have to change the tech trees at some point, but first I'm going to do a bit of research of technology development in this era before we get started on this. 
If you want to take a look at the technologies, however, they're in common/technologies.

-Country OOBs. These files in the units folder are esentially what makes up each country's military. Several things are going to be done to this:
First, we need to change all of the TAG_2000 names to TAG_1985. Next, we need to go to each country's history files and update the OOB names there. Finally, we will have to test the mod to see if there are any conflicting problems with the way they are set up right now, and get rid of the issues if there are any.
100% accurate unit placement is not on the priority list for now, so it's just making sure we have working OOBs for now.

- Add RUS (Russia) as a seperate country and events to go with for the transfer between RUS ans SOV

- Remove all Millenium Dawn items (national spirits, events etc.)

Here's a few things below that aren't on the priority list, but work can be done on them:
-Ideology icons. Probably the least important thing, but I honestly have no idea what to design for some of the ideology icons.
-News events for important things that happened in the era, like the olympic games or something like that. Definitely not necessary at the moment.

Fixes TODO:
- Add Ukrainian core to Lwow [COMPLETE]
- Remove Taiwan core from Tannu Tuva (Maybe) [COMPLETE]
- Fix crash where a certain national focus is hovered over (Some close to isolation) (Fixed due to misnamed neo_socialism)